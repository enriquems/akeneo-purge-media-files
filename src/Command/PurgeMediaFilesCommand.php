<?php

namespace I4\Akeneo\PurgeMediaFilesBundle\Command;

use Akeneo\Pim\Enrichment\Component\FileStorage;
use Akeneo\Pim\Enrichment\Component\Product\Model\Product;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModel;
use Akeneo\Pim\Enrichment\Component\Product\Repository\ProductModelRepositoryInterface;
use Akeneo\Pim\Enrichment\Component\Product\Repository\ProductRepositoryInterface;
use Akeneo\Tool\Component\FileStorage\FilesystemProvider;
use Akeneo\Tool\Component\FileStorage\Model\FileInfoInterface;
use Akeneo\Tool\Component\FileStorage\Repository\FileInfoRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use League\Flysystem\Adapter\Local;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class PurgeMediaFilesCommand
 */
class PurgeMediaFilesCommand extends Command
{
    /** @var string $defaultName */
    protected static $defaultName =  'i4:media:purge-files';

    /** @var EntityManager */
    protected $entityManager;

    /** @var FileInfoRepositoryInterface */
    protected $fileInfoRepository;

    /** @var FilesystemProvider */
    protected $filesystemProvider;

    /** @var FilesystemInterface */
    protected $filesystem;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /** @var ProductModelRepositoryInterface */
    protected $productModelRepository;

    /** @var int */
    protected $chunkSize = 500;

    /**
     * @param EntityManager $entityManager
     * @param FileInfoRepositoryInterface $fileInfoRepository
     * @param FilesystemProvider $filesystemProvider
     * @param ProductRepositoryInterface $productRepository
     * @param ProductModelRepositoryInterface $productModelRepository
     */
    public function __construct(
        EntityManager $entityManager,
        FileInfoRepositoryInterface $fileInfoRepository,
        FilesystemProvider $filesystemProvider,
        ProductRepositoryInterface $productRepository,
        ProductModelRepositoryInterface $productModelRepository,
        int $chunkSize
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->fileInfoRepository = $fileInfoRepository;
        $this->filesystemProvider = $filesystemProvider;
        $this->productRepository = $productRepository;
        $this->productModelRepository = $productModelRepository;
        $this->chunkSize = $chunkSize;
    }

    protected function configure()
    {
        $this
            ->setDescription('Remove unused product media files')
            ->addOption('delete', 'd', InputOption::VALUE_NONE, 'Remove files otherwise only lists deletions')
            ->addOption('unlinked', 'l', InputOption::VALUE_NONE, 'Remove unlinked media file (no record in database for those files)')
            ->addOption('unused', 'u', InputOption::VALUE_NONE, 'Remove unrelated media file (file no related to any product or product model)')
            ->addOption('chunk-size', 'c', InputOption::VALUE_OPTIONAL, 'Chunk the processes in blocks of this size');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws FileNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputOutput = new SymfonyStyle($input, $output);
        $this->filesystem = $this->filesystemProvider->getFilesystem(FileStorage::CATALOG_STORAGE_ALIAS);
        $delete = $input->getOption('delete');
        if (!$delete) {
            $inputOutput->warning('Command running in safe mode. Use --delete to delete files.');
        }
        /** @var int $chunkSize */
        $chunkSize = (int) $input->getOption('chunk-size')??$this->chunkSize;
        $inputOutput->writeln('Searching media files...');
        $catalogMediaFiles = $this->getCatalogMediaFiles();
        $inputOutput->writeln(sprintf('%d media files found', count($catalogMediaFiles)));
        $inputOutput->newLine();
        /** @var int $deletedFilesCount */
        $deletedFilesCount = 0;
        if(!$input->getOption('unlinked') && !$input->getOption('unused') || $input->getOption('unlinked')){
            // Delete files not linked to any product nor product model
            $inputOutput->writeln('Removing media files with no database entry...');
            $deletedFilesCount += $this->chunkProcess('removeMediaUnlinked', $chunkSize, $catalogMediaFiles, $delete, $inputOutput);
        }
        if(!$input->getOption('unlinked') && !$input->getOption('unused') || $input->getOption('unused')) {
            // Delete files which do not have any row in "akeneo_file_storage_file_info"
            $inputOutput->writeln('Removing media files without product or product model related...');
            $deletedFilesCount += $this->chunkProcess('removeMediaUnused', $chunkSize, $catalogMediaFiles, $delete, $inputOutput);
        }
        $inputOutput->success(sprintf('Total files deleted %d', $deletedFilesCount));
    }

    /**
     * @param string $method
     * @param int $chunkSize
     * @param mixed ...$params
     * @return mixed
     */
    protected function chunkProcess(string $method, int $chunkSize = 1, ...$params){
        /** @var int $totalRecords */
        $totalRecords = count($params[0]);
        if($totalRecords <= $chunkSize){
            return $this->$method(...$params);
        }
        /** @var array $chunked */
        $chunked = array_chunk($params[0], $chunkSize);
        /** @var int $totalBlocks */
        $totalBlocks = count($chunked);
        /** @var SymfonyStyle $inputOutput */
        $inputOutput = $params[2];
        /** @var int $deleteFilesCount */
        $deleteFilesCount = 0;
        $inputOutput->writeln(sprintf('%d records chunked in %d blocks of size %d', $totalRecords, $totalBlocks, $chunkSize));
        foreach($chunked as $i => $block){
            $params[0] = $block;
            $deleteFilesCount += $this->$method(...$params);
            $inputOutput->writeln(sprintf('Block %d of %d processed ;)', ($i+1), $totalBlocks));
        }
        return $deleteFilesCount;
    }


    /**
     * @param array $catalogMediaFiles
     * @param bool $delete
     * @param SymfonyStyle|null $inputOutput
     * @return int
     * @throws FileNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function removeMediaUnused(array $catalogMediaFiles, bool $delete = false, SymfonyStyle $inputOutput = null){
        /** @var int $deletedFilesCount */
        $deletedFilesCount = 0;
        $filesNotLinkedToProducts = array_filter($catalogMediaFiles, function($file) {
            /** @var FileInfoInterface $fileInfo */
            $fileInfo = $file['info'];
            return $fileInfo !== null &&
                $fileInfo->getStorage() === FileStorage::CATALOG_STORAGE_ALIAS &&
                !$this->isFileUsedInProducts($fileInfo->getKey()) &&
                !$this->isFileUsedInProductModels($fileInfo->getKey());
        });
        if(count($filesNotLinkedToProducts) > 0){
            foreach ($filesNotLinkedToProducts as $file) {
                if ($delete) {
                    $this->entityManager->remove($file['info']);
                    $this->filesystem->delete($file['file']['path']);
                    $this->entityManager->flush();
                }
                $deletedFilesCount++;
                if($inputOutput){
                    $inputOutput->writeln(sprintf('"%s" files deleted', $file['path']));
                }
            }
            if($inputOutput){
                $inputOutput->writeln(sprintf('%d files not linked to any product deleted', count($filesNotLinkedToProducts)));
            }
        }elseif($inputOutput){
            $inputOutput->writeln('There are not unlinked files to delete!');
        }
        return $deletedFilesCount;
    }

    /**
     * @param array $catalogMediaFiles
     * @param bool $delete
     * @param SymfonyStyle|null $inputOutput
     * @return int
     * @throws FileNotFoundException
     */
    protected function removeMediaUnlinked(array $catalogMediaFiles, bool $delete = false, SymfonyStyle $inputOutput = null){
        /** @var int $deletedFilesCount */
        $deletedFilesCount = 0;
        $filesWithoutDbEntry = array_filter($catalogMediaFiles, function($file) {
            return $file['info'] === null;
        });
        if(count($filesWithoutDbEntry) > 0){
            foreach ($filesWithoutDbEntry as $file) {
                if ($delete) {
                    $this->filesystem->delete($file['file']['path']);
                }
                $deletedFilesCount++;
                if($inputOutput){
                    $inputOutput->writeln(sprintf('"%s" deleted', $file['path']));
                }
            }
            if($inputOutput){
                $inputOutput->writeln(sprintf('%d files without database entry deleted!', count($filesWithoutDbEntry)));
            }
        }elseif($inputOutput){
            $inputOutput->writeln('There are not files without database entry to delete!');
        }
        return $deletedFilesCount;
    }

    /**
     * @return array
     */
    protected function getCatalogMediaFiles()
    {
        $dirContent = $this->filesystem->listContents('', true);
        $files = array_filter($dirContent, function ($item) {
            return $item['type'] === 'file';
        });
        /** @var Filesystem $filesystem */
        $filesystem = $this->filesystem;
        /** @var Local $fsAdapter */
        $fsAdapter = $filesystem->getAdapter();
        $filesWithFileInfo = array_map(function ($file) use($fsAdapter) {
            return [
                'file' => $file,
                'path' => $fsAdapter->applyPathPrefix($file['path']),
                'info' => $this->fileInfoRepository->findOneByIdentifier($file['path'])
            ];
        }, $files);
        return $filesWithFileInfo;
    }

    /**
     * @param $fileKey
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    protected function isFileUsedInProducts($fileKey)
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->entityManager
                ->getRepository(Product::class)
                ->createQueryBuilder('p')
                ->select('COUNT(p.id)')
                ->where('p.rawValues LIKE :filekey')
                ->setParameter('filekey', '%' . $fileKey . '%')
                ->getQuery()
                ->getSingleScalarResult() > 0;
    }

    /**
     * @param $fileKey
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    protected function isFileUsedInProductModels($fileKey)
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->entityManager
                ->getRepository(ProductModel::class)
                ->createQueryBuilder('p')
                ->select('COUNT(p.id)')
                ->where('p.rawValues LIKE :filekey')
                ->setParameter('filekey', '%' . $fileKey . '%')
                ->getQuery()
                ->getSingleScalarResult() > 0;
    }
}
