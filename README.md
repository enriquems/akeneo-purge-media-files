# Interactiv4 Akeneo Bundle "Purge Media Files"

## Description
Command to remove unused media files.

## Minimum Compatibility
- PHP: ~7.3
- Akeneo CE: ~4.0

## Installation Instructions
You can install this package using composer by adding it to your composer file using following command:

`composer require interactiv4/akeneo-purge-media-files`

Add to config/bundles.php:

`I4\Akeneo\PurgeMediaFiles\I4AkeneoPurgeMediaFilesBundle::class => ['env' => true],`

## Credits
Supported and maintained by Interactiv4 Team.

## License
Do not distribute or share this code unless you are authorized to do so.

## Copyright
Copyright (c) Interactiv4 S.L.